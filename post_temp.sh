#!/usr/bin/sh

URL="http://www.golem.de/projekte/ot/temp.php?dbg=0&type=sbc&country=DE&zip=201469&city=Hamburg&lat=53.56686&long=9.98409&temp="

while true; do
    DEGREE=`python ./tempyrature`
    echo ${DEGREE}
    curl -silent "${URL}${DEGREE}" > /dev/null
    sleep 60
done
