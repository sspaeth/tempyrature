Tempyrature
==========

A command line utility to read out the temperature from a "temper
v1.4". This tool works on Windows with the pywinusb as dependency. In
contrast to other tools, it thus provides a python-only solution
without the need to compile .dll's as part of their scripts and
dependencies.

Requirements:
 - Python3
 - pywinusb (on Windows)("pip install pywinusb")

It is licensed under the GNU GPL v3+.

Usage
---------

see ./tempyrature --help

Exit codes are 0 on success and 1 if no sensor was found.
